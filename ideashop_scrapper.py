import requests
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor


def get_list_of_product_links(url):
    product_links = []
    html_text = requests.get(url).text
    soup = BeautifulSoup(html_text, 'lxml')
    main_categories = soup.find('div', class_="sidebar_menu_inner").find('ul', class_="menu").find_all('li', class_="full")
    main_categories.pop(0)
    for main_category in main_categories:
        links = main_category.find_all('a', class_="icons_fa parent")
        for link in links:
            main_category_link = url + link['href']
            sub_html_text = requests.get(main_category_link).text
            sub_soup = BeautifulSoup(sub_html_text, 'lxml')
            # to count number of pages in a given category
            pages = sub_soup.find('div', class_="module-pagination")
            if pages:
                pages_count = int(pages.find_all('a')[-1].text) + 1
            else:
                pages_count = 2
            for i in range(1, pages_count):
                linkself = main_category_link + '?PAGEN_1=' + str(i)
                link_text = requests.get(linkself).text
                link_soup = BeautifulSoup(link_text, 'lxml')
                product_divs = link_soup.find_all('div', class_="item_info")
                for product_div in product_divs:
                    if product_div.find('div', class_='item-stock').find('span', class_='value').text == 'В наличии':
                        half_link = product_div.find('div', class_='item-title').a['href']
                        product_link = url + half_link
                        product_links.append(product_link)
    return product_links


def parse_product_page(product_link):
    product_html = requests.get(product_link).text
    product_soup = BeautifulSoup(product_html, 'lxml')
    name = product_soup.find('div', class_='topic__heading').h1.text.strip()
    price = product_soup.find('span', class_='values_wrapper').find('span', class_='price_value').text.replace(u'\xa0', u' ')
    categories = product_soup.find('div', {'id': 'navigation'})
    main_category = categories.find('div', {'id' :'bx_breadcrumb_2'}).find('span', class_='breadcrumbs__item-name').text
    sub_category = categories.find('div', {'id': "bx_breadcrumb_3"}).find('span', class_='breadcrumbs__item-name').text
    if not categories.find('div', {'id': "bx_breadcrumb_4"}):
        self_category = sub_category
    else:
        self_category = categories.find('div', {'id': "bx_breadcrumb_4"}).find('span', class_='breadcrumbs__item-name').text
    if not product_soup.find('div', class_='properties list'):
        description = 'No Description'
    elif not product_soup.find('div', class_='tab-content').find('div', {'id': 'props'}):
        description_titles = [i.text.strip() for i in product_soup.find('div', class_='properties list').find_all('div', class_='properties__title')]
        description_values = [i.text.strip() for i in product_soup.find('div', class_='properties list').find_all('div', class_='properties__value')]
        description = f'{dict(zip(description_titles, description_values))}'
    else:
        description_titles = [i.text.strip() for i in product_soup.find('div', class_='tab-content').find('div', {'id': 'props'}).find_all('td', class_='char_name')]
        description_values = [i.text.strip() for i in product_soup.find('div', class_='tab-content').find('div', {'id': 'props'}).find_all('td', class_='char_value')]
        description = f'{dict(zip(description_titles, description_values))}'

    product_images = ['https://ideashop.kg'+ i['src'] for i in product_soup.find('div', class_='product-detail-gallery__container').find_all('img', class_='lazy') if not 'noimage_product.svg' in i['src']]

    product = {
        'Главная категория': main_category,
        'Субкатегория': sub_category,
        'Категория продукта': self_category,
        'Название продукта': name,
        'Цена продукта': price,
        'Описание товарa': description,
        'Список изображений': product_images,
        'Ссылка на продукт': product_link
        }

    return product


executor = ThreadPoolExecutor(max_workers=10)


def main():
    url = 'https://ideashop.kg'
    print('Preparing links of end products. It takes usually around 6-7 minutes... Take a Coffee Break! :)')
    product_links = get_list_of_product_links(url=url)
    products = [parse_product_page(i) for i in product_links]
    print(len(product_links))
    print(len(products))
    return products


main()


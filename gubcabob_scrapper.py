import requests
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor


executor = ThreadPoolExecutor(max_workers=10)


def get_product_category_links(url):
    main_html = requests.get(url).text
    main_soup = BeautifulSoup(main_html, 'lxml')
    categories = main_soup.find('div', {'id': 'my-menu'}).find_all('a', class_='dropdown-item')
    list_of_category_links = [i['href'] for i in categories]
    remove_indexes = 0, 7, 11, 25, 40, 56, 68
    category_links  = [i for j, i in enumerate(list_of_category_links) if j not in remove_indexes]
    return category_links


def parse_links(category_link):
    category_html = requests.get(category_link).text
    category_soup = BeautifulSoup(category_html, 'lxml')
    category_section = category_soup.find('div', class_='elementor-section-wrap').find_all('section')[0]
    bread_crumb = category_section.find('div', class_='elementor-widget-container').find('nav', class_='woocommerce-breadcrumb')
    main_category = bread_crumb.find_all('a')[1].text
    self_category = list(bread_crumb.children)[-1].text.replace(u'\xa0/\xa0', u'')
    if len(bread_crumb.find_all('a')) == 3:
        sub_category= bread_crumb.find_all('a')[2].text
    else:
        sub_category = self_category
    
    product_section = category_soup.find('div', class_='elementor-section-wrap').find_all('section')[-1]
    if product_section.find('div', class_='elementor-widget-container'):
        product_divs = product_section.find('div', class_='elementor-widget-container').find('ul').find_all('li')
        
        category_products = [
            {
            'Главная категория': main_category,
            'Субкатегория': sub_category,
            'Категория продукта': self_category,
            'Название продукта': i.find('h2', class_='woocommerce-loop-product__title').text,
            'Цена продукта': i.find('span', class_='price').find('bdi').text.replace(u'\xa0сом', u''),
            'Описание товарa': 'No description',
            'Список изображений': [i.find('a', class_='woocommerce-LoopProduct-link').img['src']]}
            for i in product_divs
            ]
        return category_products


def main():
    url = 'https://gubkabob.kg/'
    list_of_category_links = get_product_category_links(url=url)

    '''
    returns iterable of each category's products in a list, so final return is iterable of lists
    list comprehension is used to remove empty lists, where there was no product on a given category
    '''
    category_products = [i for i in list(executor.map(parse_links, list_of_category_links)) if i]

    # to unpack list of list of products
    list_of_products = [item for sublist in category_products for item in sublist]
    return list_of_products


main()

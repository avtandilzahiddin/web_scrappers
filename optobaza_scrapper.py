import requests, re
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor

executor = ThreadPoolExecutor(max_workers=10)

def get_list_of_main_categories(url):
    html_text = requests.get(url).text
    soup = BeautifulSoup(html_text, 'lxml')
    main_categories = soup.find_all('li', class_="static-lvl")
    main_category_links = [url + i.a['href'] for i in main_categories]
    return main_category_links

def get_list_of_product_links(main_category_link):
    product_links = []
    sub_html_text = requests.get(main_category_link).text
    sub_soup = BeautifulSoup(sub_html_text, 'lxml')
    pages = sub_soup.find('div', class_='product-list-nav').find('div', class_='flex-form').find('input', class_='page_input')['max'] 
    pages_count = int(pages) + 1
    for i in range(1, pages_count):
        link_text = requests.get(main_category_link + '?page=' + str(i)).text
        link_soup = BeautifulSoup(link_text, 'lxml')
        product_divs = link_soup.find('div', class_='grid-product column').find_all('div', class_='info')
        for product_div in product_divs:
            if product_div.find('div', class_='availability yes'):
                product_link = 'https://optobaza.kg' + product_div.find('a')['href']
                product_links.append(product_link)
    return product_links


def parse_product_page(product_link):
    product_html = requests.get(product_link).text
    product_soup = BeautifulSoup(product_html, 'lxml')
    category = product_soup.find('div', class_='crumb-urls').find_all('a')[-1].text.strip()
    name = product_soup.find('div', class_='crumb-urls').find('span').text.strip()
    price = product_soup.find('div', class_='new-price').text.strip()
    description = product_soup.find('div', class_='ck-editor-text').text.replace('Описание', '').strip()
    product_images = 'https://optobaza.kg' + product_soup.find('div', class_='image').find('img')['src']

    product = {
        'Категория': category,
        'Название продукта': name,
        'Цена продукта': int(re.sub("[^0-9]", "", price)),
        'Описание товарa': description,
        'Список изображений': product_images,
        'Ссылка на продукт': product_link
        }
    print(product)
    print('\n')
    return product

def main():
    url = 'https://optobaza.kg'
    print('Preparing links of end products. It takes usually around 16 seconds...')
    main_category_links = get_list_of_main_categories(url=url)
    #раскрыть список из списков
    product_links = [item for sublist in executor.map(get_list_of_product_links, main_category_links) for item in sublist]
    products = list(executor.map(parse_product_page, product_links))
    print(products)
    print(len(products))
    return products

main()

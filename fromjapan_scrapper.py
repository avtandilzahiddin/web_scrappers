from bs4 import BeautifulSoup
import requests
from concurrent.futures import ThreadPoolExecutor


def get_list_of_main_categories(url):
    html_text = requests.get(url).text
    soup = BeautifulSoup(html_text, 'lxml')
    main_categories = soup.find_all('li', class_="main-menu__act")
    return main_categories


def get_list_of_product_links(main_category):
    list_of_product_links = []
    main_category_link = main_category.a['href']
    sub_html_text = requests.get(main_category_link).text
    sub_soup = BeautifulSoup(sub_html_text, 'lxml')
    # to count number of pages in a given category
    pages = sub_soup.find('div', class_='pagination')
    if pages:
        pages_count = len(pages.find_all('li')) + 1
    else:
        pages_count = 2
    for i in range(1, pages_count):
        link_text = requests.get(main_category_link + '?pages=' + str(i)).text
        link_soup = BeautifulSoup(link_text, 'lxml')
        product_divs = link_soup.find('div', class_="catalog").find_all('div', class_='list-slider__slide')
        # list of end products
        for product_div in product_divs:
            if not product_div.find('span', class_='product-name not_available'):
                product_link = product_div.find('a', class_='mask')['href']
                list_of_product_links.append(product_link)
    return list_of_product_links


def parse_product_page(product_link):
    product_html = requests.get(product_link).text
    product_soup = BeautifulSoup(product_html, 'lxml')
    name = product_soup.find('div', class_='page__name').h2.text
    price = product_soup.find('div', class_='price').h6.text
    bread_scrumbs = product_soup.find('ul', class_="bread-crumbs").find_all('li')
    biggest_category = bread_scrumbs[1].a.text
    sub_category = bread_scrumbs[2].a.text
    if not bread_scrumbs[3].a:
        self_category = sub_category
    else:
        self_category = bread_scrumbs[3].a.text
    if not product_soup.find('div', class_='product__text').p:
        description = 'No Description'
    else:
        description = product_soup.find('div', class_='product__text').p.text
    product_images = [i['href'] for i in product_soup.find('div', class_="product__left").find_all('a')]

    product = {
        'Главная категория': biggest_category,
        'Субкатегория': sub_category,
        'Категория продукта': self_category,
        'Название продукта': name,
        'Цена продукта': price,
        'Описание товарa': description,
        'Список изображений': product_images,
        'Ссылка на продукт': product_link
        }
    print(product)
    print('\n')
    return product


executor = ThreadPoolExecutor(max_workers=10)


def main():
    url = "https://fromjapan.kg/"
    print('Preparing links of end products. It takes usually around 30 seconds...')
    #unpacks list of lists
    list_of_links = [item for sublist in executor.map(get_list_of_product_links, get_list_of_main_categories(url=url)) for item in sublist]
    products = list(executor.map(parse_product_page, list_of_links))
    return products


main()
